package com.kborodin.paycasso

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.kborodin.paycasso.databinding.FragmentMainBinding
import com.kborodin.paycasso.di.Injectable
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment(), Injectable, View.OnClickListener {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentMainBinding.inflate(inflater, container, false)
        context ?: return binding.root
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        btnStart.setOnClickListener(this)
        btnList.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            btnStart -> view?.findNavController()?.navigate(MainFragmentDirections.actionMainFragmentToCameraFragment())
            btnList -> view?.findNavController()?.navigate(MainFragmentDirections.actionMainFragmentToPhotosFragment())
        }
    }

}