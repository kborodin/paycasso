package com.kborodin.paycasso.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
