package com.kborodin.paycasso.di

import com.kborodin.paycasso.MainFragment
import com.kborodin.paycasso.camera.ui.CameraFragment
import com.kborodin.paycasso.photos.PhotosFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeMainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributePicturesFragment(): PhotosFragment

    @ContributesAndroidInjector
    abstract fun contributeCameraFragment(): CameraFragment

}
