package com.kborodin.paycasso.photos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.kborodin.paycasso.databinding.FragmentPhotosBinding
import com.kborodin.paycasso.di.Injectable

class PhotosFragment : Fragment(), Injectable {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentPhotosBinding.inflate(inflater, container, false)
        context ?: return binding.root
        return binding.root
    }
}