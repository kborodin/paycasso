package com.kborodin.paycasso.camera.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "photos")
class Photo(
    @PrimaryKey(autoGenerate = true)
    @field:SerializedName("id")
    val id: Int,
    @field:SerializedName("uri")
    val uri: String,
    @field:SerializedName("timestamp")
    val timestamp: Date
) {
    override fun toString(): String = uri
}

