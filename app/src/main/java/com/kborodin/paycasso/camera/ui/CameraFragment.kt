package com.kborodin.paycasso.camera.ui

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.kborodin.paycasso.R
import com.kborodin.paycasso.databinding.FragmentCameraBinding
import com.kborodin.paycasso.di.Injectable
import kotlinx.android.synthetic.main.fragment_camera.*
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.CameraBridgeViewBase
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.core.Mat
import org.opencv.core.MatOfRect
import org.opencv.core.Point
import org.opencv.core.Scalar
import org.opencv.imgproc.Imgproc
import org.opencv.objdetect.CascadeClassifier
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream


class CameraFragment : Fragment(), Injectable, CameraBridgeViewBase.CvCameraViewListener2 {

    private var cascadeClassifier: CascadeClassifier? = null
    private lateinit var baseLoaderCallback: BaseLoaderCallback

    private var mCascadeFile: File? = null
    private var rgba: Mat? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentCameraBinding.inflate(inflater, container, false)
        context ?: return binding.root
        return binding.root
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        openCvCameraView.setCvCameraViewListener(this)
        initBaseLoaderCallback()
    }

    private fun initBaseLoaderCallback() {
        baseLoaderCallback = object : BaseLoaderCallback(activity!!) {
            override fun onManagerConnected(status: Int) {
                when (status) {
                    LoaderCallbackInterface.SUCCESS -> {
                        initializeOpenCVDependencies()
                    }
                    else -> {
                        super.onManagerConnected(status)
                    }
                }
            }
        }
    }

    private fun initializeOpenCVDependencies() {
        try { // Copy the resource into a temp file so OpenCV can load it
            val `is`: InputStream = resources.openRawResource(R.raw.lbpcascade_frontalface_improved)
            val cascadeDir = activity!!.getDir("cascade", Context.MODE_PRIVATE)
            mCascadeFile = File(cascadeDir, "lbpcascade_frontalface_improved.xml")
            val os = FileOutputStream(mCascadeFile!!)
            val buffer = ByteArray(4096)
            var bytesRead: Int

            while (`is`.read(buffer).also { bytesRead = it } != -1) {
                os.write(buffer, 0, bytesRead)
            }
            `is`.close()
            os.close()
            // Load the cascade classifier
            cascadeClassifier = CascadeClassifier(mCascadeFile!!.absolutePath)
            if (cascadeClassifier!!.empty()) {
                cascadeClassifier = null
            } else {
                cascadeDir.delete()
            }
        } catch (e: Exception) {
            Log.e("OpenCVActivity", "Error loading cascade", e)
        }
        // And we are ready to go
        openCvCameraView.enableView()
    }

    override fun onResume() {
        super.onResume()
        if (!OpenCVLoader.initDebug()) {
            Toast.makeText(activity!!, "Can't load camera, please try again", Toast.LENGTH_SHORT)
                .show()
        } else {
            baseLoaderCallback.onManagerConnected(BaseLoaderCallback.SUCCESS)
        }
    }

    override fun onCameraViewStarted(width: Int, height: Int) {
        rgba = Mat()
    }

    override fun onCameraViewStopped() {
        rgba?.release()
    }

    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame?): Mat {
        rgba = inputFrame?.rgba()
        //detect face
        val faces = MatOfRect()
        if (cascadeClassifier != null) {
            cascadeClassifier!!.detectMultiScale(rgba, faces)
        }

        faces.toArray().forEach {
            Imgproc.rectangle(
                rgba,
                Point(it.x.toDouble(), it.y.toDouble()),
                Point(it.x + it.width.toDouble(), it.y + it.height.toDouble()),
                Scalar(255.0, 0.0, 0.0)
            )
        }

        return rgba!!
    }

    override fun onDestroy() {
        super.onDestroy()
        openCvCameraView?.disableView()
    }
}